#include <stdio.h>
#include <string.h>
#include "../common/HPMi.h"
#include "../map/script.h"
#include "../map/pc.h"
#include "../map/clif.h"
#include "../common/HPMDataCheck.h"
 
HPExport struct hplugin_info pinfo = {
	"dispbottom2",		// Plugin name
	SERVER_TYPE_MAP,// Which server types this plugin works with?
	"1.0",			// Plugin version
	HPM_VERSION,	// HPM Version (don't change, macro is automatically updated)
};
 
/*==========================================
 * Coloured DispBottom [By Dastgir]
 *------------------------------------------*/
BUILDIN(dispbottom2)
{
    TBL_PC *sd=script->rid2sd(st);
    const char *message;
    unsigned long color;
    message=script_getstr(st,3);
    color=strtoul(script_getstr(st,2),NULL,0);
    if(sd)
        clif_colormes_e(sd,color,message);
    return 0;
}

// [By Dastgir]
int clif_colormes_e(struct map_session_data * sd,unsigned long color1, const char* msg) {
    
    unsigned short msg_len = strlen(msg) + 1;
    WFIFOHEAD(sd->fd,msg_len + 12);
    WFIFOW(sd->fd,0) = 0x2C1;
    WFIFOW(sd->fd,2) = msg_len + 12;
    WFIFOL(sd->fd,4) = 0;
    WFIFOL(sd->fd,8) = (color1&0x0000FF) << 16 | (color1&0x00FF00) | (color1&0xFF0000) >> 16; // RGB -> BGR
    safestrncpy((char*)WFIFOP(sd->fd,12), msg, msg_len);
    clif->send(WFIFOP(sd->fd,0), WFIFOW(sd->fd,2), &sd->bl, SELF);


    return 0;
}

 
/* Server Startup */
HPExport void plugin_init (void)
{
    clif = GET_SYMBOL("clif");
    script = GET_SYMBOL("script");
    pc = GET_SYMBOL("pc");
	
	addScriptCommand("dispbottom2","ss",dispbottom2);
}

/* run when server is ready (online) */
HPExport void server_online (void) {
	ShowInfo("Dispbottom2 plugin is loaded.");
}