-	script	NoviceLogin	-1,{
	
	OnPCLoginEvent:
	if(!#nLoginIn) {
		mes .npcName$;
		mes "������������ ��� �� ����� �������!";
		mes "� ����� ����� ������� ��������� �������.";
		next;
		mes .npcName$;
		mes "�������: � ����� ��������� � �������� ������ ������ ��������� �� ��������.";
		next;
		mes .npcName$;
		mes "�������� ���������: ";
		switch(select("Swordsman:Mage:Archer:Merchant:Thief:Acolyte:Ninja:Taekwon:Gunslinger:Super Novice")) {
			case 1: // Swordsman
			next;
			mes .npcName$;
			callfunc "New_Player", 1, 15, 10, 5000, 0, 0, 7227, 1, 0, 0, 0, 0, 0, 0;
			mes "������ ����� ��� �� ����� �������!";
			close;
			
			case 2: // Mage
			next;
			mes .npcName$;
			callfunc "New_Player", 2, 15, 10, 5000, 0, 0, 7227, 1, 0, 0, 0, 0, 0, 0;
			mes "������ ����� ��� �� ����� �������!";
			close;
			
			case 3: // Archer
			next;
			mes .npcName$;
			callfunc "New_Player", 3, 15, 10, 5000, 0, 0, 7227, 1, 0, 0, 0, 0, 0, 0;
			mes "������ ����� ��� �� ����� �������!";
			close;
			
			case 4: // Merchant
			next;
			mes .npcName$;
			callfunc "New_Player", 5, 15, 10, 5000, 0, 0, 7227, 1, 0, 0, 0, 0, 0, 0;
			mes "������ ����� ��� �� ����� �������!";
			close;
			
			case 5: // Thief
			next;
			mes .npcName$;
			callfunc "New_Player", 6, 15, 10, 5000, 0, 0, 7227, 1, 0, 0, 0, 0, 0, 0;
			mes "������ ����� ��� �� ����� �������!";
			close;
			
			case 6: // Acolyte
			next;
			mes .npcName$;
			callfunc "New_Player", 4, 15, 10, 5000, 0, 0, 7227, 1, 0, 0, 0, 0, 0, 0;
			mes "������ ����� ��� �� ����� �������!";
			close;
			
			case 7: // Ninja
			next;
			mes .npcName$;
			callfunc "New_Player", 25, 15, 10, 5000, 0, 0, 7227, 1, 0, 0, 0, 0, 0, 0;
			mes "������ ����� ��� �� ����� �������!";
			close;
			
			case 8: // Taekwon
			next;
			mes .npcName$;
			callfunc "New_Player", 4046, 15, 10, 5000, 0, 0, 7227, 1, 0, 0, 0, 0, 0, 0;
			mes "������ ����� ��� �� ����� �������!";
			close;
			
			case 9: // Gunslinger
			next;
			mes .npcName$;
			callfunc "New_Player", 24, 15, 10, 5000, 0, 0, 7227, 1, 0, 0, 0, 0, 0, 0;
			mes "������ ����� ��� �� ����� �������!";
			close;
			
			case 10: // Super Novice
			next;
			mes .npcName$;
			callfunc "New_Player", 23, 15, 10, 5000, 0, 0, 7227, 1, 0, 0, 0, 0, 0, 0;
			mes "������ ����� ��� �� ����� �������!";
			close;
			
		}
	} else end;

	OnInit:
	.npcName$ = "[^0000FF�������^000000]";
	end;
}

/*
	Give players stuff (C) Oxxy, 2015, v 1.0
	
	v1.0 Initial release
	
	Syntax:
	callfunc "New_Player","<VARIABLE>", <JOB_ID>, <BASE_LEVELS>, <JOB_LEVELS>, <ZENY>, <STATUS_POINTS>, <SKILL_POINTS>, <I1>,<I1A>, <I2>,<I2A>, <I3>,<I3A>, <I4>,<I4A>;
	
	Example usage:
	callfunc "New_Player","#first_login", 1, 15, 10, 5000, 0, 0, 4001, 1, 7227, 2, 607, 3, 0, 0;
	
	example above will set account variable #first_login to 1, give player Swordman job, 15 base levels, 10 job levels, 5000 zeny, 0 status points, 0 skill points, and items Poring card x1, TCG Card x2, Ygg. berry x3 and the 4th item won't be given.
*/
function	script	New_Player	{
																					
	.Job = getarg(0);		// NPC will give this JOB to player
	.bLevel = getarg(1);	// NPC will give this much of base levels.
	.jLevel = getarg(2);	// NPC will give this much of job levels
	.Zeny = getarg(3);		// Amount of Zeny given, if 0 - no zeny will be given
	.stPoints = getarg(4);	// Set this to amount of Status points that you want to give to player
	.skPoints = getarg(5);	// Set this to amount of Skill points that you want to give to player
	setarray .items[0], 
	getarg(6), getarg(7),	// Item ID 1, Item ID 1 Amount
	getarg(8), getarg(9), 	// Item ID 2, Item ID 2 Amount
	getarg(10), getarg(11),	// Item ID 3, Item ID 3 Amount
	getarg(12), getarg(13);	// Item ID 4, Item ID 4 Amount
							// Add more if u want to.
		
	// Defaulting values	
	if(.bLevel < 0) .bLevel = 0;				// You shouldn't take levels from player?
	if(.jLevel < 0) .jLevel = 0;				// See above.
		
	if(.stPoints < 0) .stPoints = 0;			// You shouldn't take status points from player?
	if(.skPoints < 0) .skPoints = 0;			// See above
		
	if(.Zeny < 0) .Zeny = 0;					// You shouldn't take zeny from player?
		
	// Function
	#nLoginIn = 1;
	
	if(.Job) jobchange .Job;
	
	if(.bLevel) BaseLevel = .bLevel;
	if(.jLevel) JobLevel = .jLevel;
	
	if(.Zeny) Zeny += .Zeny;
	if(getarraysize(.items)) {
		if(.items[0] && .items[1])
			getitem .items[0], .items[1];
		if(.items[2] && .items[3])
			getitem .items[2], .items[3];
		if(.items[4] && .items[5])
			getitem .items[4], .items[5];
		if(.items[6] && .items[7])
			getitem .items[6], .items[7];
	}
	
	if(.stPoints)
		StatusPoint += .stPoints;
	if(.skPoints)
		SkillPoint += .skPoints;
		
	// Return to original script
	return;
		
}